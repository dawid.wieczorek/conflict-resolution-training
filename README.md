# Trening rozwiązywania konfliktów

Na tym repozytorium znajdują się cztery branche które zawierają konflikty.
W ramach treningu należy:

1. skopiwać repozytorium na swój profil (przycisk "Fork" na GitLabie)
2. sklonować repozytorium na lokalne środowisko
3. zmerdżować wszystkie zmiany z branchy do brancza integration. Jeśli pojawią się konflikty, rozwiąż je

Spróbuj wykorzystac komendy merge jak i rebase.

Kolejność integracji zmian nie ma znaczenia. Finalnie ma powstać zrozumiały przepis na pizze, ostateczną treść zostawiam Tobie do oceny. 

Jeśli wystąpią jakieś problemy z rozwiązaniem konfliktów lub masz jakieś pytania,
proszę o kontakt na Teamsach.
